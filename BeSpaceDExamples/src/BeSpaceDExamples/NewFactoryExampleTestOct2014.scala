/* 
 * J O Blech 2014
 * 
 * Created 10/10/2014
 * may need runtime setting -Xss515m or similar,  */

package BeSpaceDExamples

import BeSpaceDCore._;


object NewFactoryExampleTestOct2014 extends CoreDefinitions {
	//factory hall
	def FactoryHall = IMPLIES(Owner("FactoryHall"),OccupyBox(50,50,250,150))
	//factory hall + adjacent area
	def FactoryHallAdjArea = IMPLIES(Owner("FactoryHallAdjArea"),OccupyBox(20,20,280,180)) // + 30 in each direction
	//factory hall + core area
	def FactoryHallCoreArea = IMPLIES(Owner("FactoryHallCoreArea"),OccupyBox(80,80,100,100)) // + 30 in each direction

	def FireSensor1 = IMPLIES(Owner("FireSensor1"),OccupyPoint(80,100))
	def FireSensor2 = IMPLIES(Owner("FireSensor2"),OccupyPoint(120,100))
	def FireSensor3 = IMPLIES(Owner("FireSensor3"),OccupyPoint(160,100))
	def FireSensor4 = IMPLIES(Owner("FireSensor4"),OccupyPoint(200,100))
	def FireSensor5 = IMPLIES(Owner("FireSensor5"),OccupyPoint(230,100))
	
	def fsdetrange = BIGAND (
		IMPLIES(AND(Event("FireDetection"),Prob(1.0)),OccupyCircle(80,100,10))::
		IMPLIES(AND(Event("FireDetection"),Prob(.95)),OccupyCircle(80,100,90))::		
		
		
		IMPLIES(AND(Event("FireDetection"),Prob(1.0)),OccupyCircle(120,100,10))::
		IMPLIES(AND(Event("FireDetection"),Prob(.95)),OccupyCircle(120,100,90))::	
		
		
		IMPLIES(AND(Event("FireDetection"),Prob(1.0)),OccupyCircle(160,100,10))::
		IMPLIES(AND(Event("FireDetection"),Prob(.95)),OccupyCircle(160,100,90))::	
		
		
		IMPLIES(AND(Event("FireDetection"),Prob(1.0)),OccupyCircle(200,100,10))::
		IMPLIES(AND(Event("FireDetection"),Prob(.95)),OccupyCircle(200,100,90))::	
		
		
		IMPLIES(AND(Event("FireDetection"),Prob(1.0)),OccupyCircle(230,100,10))::
		IMPLIES(AND(Event("FireDetection"),Prob(.95)),OccupyCircle(230,100,90))::	
		Nil)

	def FireExtinguisher = IMPLIES(Owner("FireExtinguisher"),OccupyPoint(100,100))
	
	//machine 1
	def machine1 = IMPLIES(Owner("Machine1"),OccupyBox(60,70,62,72));
		
	def machine1states = BIGOR(
			ComponentState("off")::
			ComponentState("normal")::
			ComponentState("broken")::
			ComponentState("onFire")::			
			Nil
	)
	
	def machine1errorstate = IMPLIES(ComponentState("broken"),Event("ToxicSpillMachine1"));
	def machine1toxicspill = BIGAND (
			IMPLIES(AND(Event("ToxicSpillMachine1"),Prob(0.9)),OccupyCircle(61,71,10))::
			IMPLIES(AND(Event("ToxicSpillMachine1"),Prob(0.1)),OccupyCircle(61,71,20))::

			Nil
	)
	
	//machine 2
	def machine2 = IMPLIES(Owner("Machine2"),OccupyBox(75,85,80,86));
		
	def machine2states = BIGOR(
			ComponentState("off")::
			ComponentState("normal")::
			ComponentState("broken")::
			ComponentState("onFire")::			
			Nil
	)
	
	def machine2errorstate = IMPLIES(ComponentState("broken"),Event("ToxicSpillMachine1"));
	def machine2toxicspill = BIGAND (
			IMPLIES(AND(Event("ToxicSpillMachine2"),Prob(0.9)),OccupyCircle(77,85,15))::
			IMPLIES(AND(Event("ToxicSpillMachine2"),Prob(0.1)),OccupyCircle(77,85,17))::

			Nil
	)	
	
	
	def stafflocation1 = IMPLIES(Owner("Charles"),OccupyPoint(20,20))
	def stafflocation2 = IMPLIES(Owner("Alice"),OccupyPoint(30,25))
	
  def main(args: Array[String]) {
	  //TODO: 10/10/2014 debug projectCondition Function
	  
	  
		//incoming event: UNKNOWN INCEDENT
		var coordinate_x : Int = 70
		var coordinate_y : Int = 60
		//println (unfoldInvariant(projectCondition(Owner("FactoryHall"),FactoryHall)))
		//println (unfoldInvariant(projectCondition(AND(Event("FireDetection"),Prob(1.0)),fsdetrange)))
		//println (unfoldInvariant(projectCondition(Owner("FactoryHallAdjArea"),FactoryHallAdjArea)))
		println(projectCondition(Owner("FactoryHall"),FactoryHall))
		println(projectCondition(AND(Event("FireDetection"),Prob(1.0)),fsdetrange))
		println ("Test: small box in big box, should be true")
		println (inclusionTestsBig(unfoldInvariant(OccupyBox(10,10,20,20))::Nil,unfoldInvariant(OccupyBox(0,0,30,30))::Nil))
		println ("Test: big box in small box, should be false")
		println (inclusionTestsBig(unfoldInvariant(OccupyBox(0,0,30,30))::Nil,unfoldInvariant(OccupyBox(10,10,20,20))::Nil))
		println ("Test: overlapping boxes should be false")
		println (inclusionTestsBig(unfoldInvariant(OccupyBox(0,0,20,20))::Nil,unfoldInvariant(OccupyBox(10,10,30,30))::Nil))
		println ("Test: disjunct boxes should be false")
		println (inclusionTestsBig(unfoldInvariant(OccupyBox(0,0,10,10))::Nil,unfoldInvariant(OccupyBox(20,20,30,30))::Nil))
		println("Some checking")
		println("Factory hall covered by firedetection prob 1.0")
		println (inclusionTestsBig(unfoldInvariant(projectCondition(Owner("FactoryHall"),FactoryHall))::Nil,unfoldInvariant(projectCondition(AND(Event("FireDetection"),Prob(1.0)),fsdetrange))::Nil))
		println("firedetection prob 1.0 covered by factory hall")
		println (inclusionTestsBig(unfoldInvariant(projectCondition(AND(Event("FireDetection"),Prob(1.0)),fsdetrange))::Nil,unfoldInvariant(projectCondition(Owner("FactoryHall"),FactoryHall))::Nil))
		println("Factory hall covered by firedetection prob 0.95")		
		println (inclusionTestsBig(unfoldInvariant(projectCondition(Owner("FactoryHall"),FactoryHall))::Nil,unfoldInvariant(projectCondition(AND(Event("FireDetection"),Prob(0.95)),fsdetrange))::Nil))
		println("firedetection prob 0.95 covered by factory hall")
		println (inclusionTestsBig(unfoldInvariant(projectCondition(AND(Event("FireDetection"),Prob(0.95)),fsdetrange))::Nil,unfoldInvariant(projectCondition(Owner("FactoryHall"),FactoryHall))::Nil))
		
		println("Extended actory hall covered by firedetection prob 1.0")		
		println (inclusionTestsBig(unfoldInvariant(projectCondition(Owner("FactoryHallAdjArea"),FactoryHallAdjArea))::Nil,unfoldInvariant(projectCondition(AND(Event("FireDetection"),Prob(1.0)),fsdetrange))::Nil))
		println("firedetection prob 1.0 covered by extended factory hall")
		println (inclusionTestsBig(unfoldInvariant(projectCondition(AND(Event("FireDetection"),Prob(1.0)),fsdetrange))::Nil,unfoldInvariant(projectCondition(Owner("FactoryHallAdjArea"),FactoryHallAdjArea))::Nil))

		println("Extended actory hall covered by firedetection prob 0.95")		

		println (inclusionTestsBig(unfoldInvariant(projectCondition(Owner("FactoryHallAdjArea"),FactoryHallAdjArea))::Nil,unfoldInvariant(projectCondition(AND(Event("FireDetection"),Prob(0.95)),fsdetrange))::Nil))

		println("firedetection prob 0.95 covered by extended factory hall")

		println (inclusionTestsBig(unfoldInvariant(projectCondition(AND(Event("FireDetection"),Prob(0.95)),fsdetrange))::Nil,unfoldInvariant(projectCondition(Owner("FactoryHallAdjArea"),FactoryHallAdjArea))::Nil))
		
		println("core Factory hall covered by firedetection prob 1.0")		
		
		println (inclusionTestsBig(unfoldInvariant(projectCondition(Owner("FactoryHallCoreArea"),FactoryHallCoreArea))::Nil,unfoldInvariant(projectCondition(AND(Event("FireDetection"),Prob(1.0)),fsdetrange))::Nil))
		println("firedetection prob 1.0 covered by core factory hall")
		println (inclusionTestsBig(unfoldInvariant(projectCondition(AND(Event("FireDetection"),Prob(1.0)),fsdetrange))::Nil,unfoldInvariant(projectCondition(Owner("FactoryHallCoreArea"),FactoryHallCoreArea))::Nil))

		println("core Factory hall covered by firedetection prob 0.95")		
		println (inclusionTestsBig(unfoldInvariant(projectCondition(Owner("FactoryHallCoreArea"),FactoryHallCoreArea))::Nil,unfoldInvariant(projectCondition(AND(Event("FireDetection"),Prob(0.95)),fsdetrange))::Nil))
		println("firedetection prob 0.95 covered by core factory hall")
		println (inclusionTestsBig(unfoldInvariant(projectCondition(AND(Event("FireDetection"),Prob(0.95)),fsdetrange))::Nil,unfoldInvariant(projectCondition(Owner("FactoryHallCoreArea"),FactoryHallCoreArea))::Nil))
		

		println("<command type='composite' image='substation9.jpg'>")
		println("<display type='rect' x='300' y='500' w='150' h='100'></display>")
		println("<display type='text' text='substation incident' x='100' y='50' color='blue'></display>")
		println("</command>")
  }
  
  
}