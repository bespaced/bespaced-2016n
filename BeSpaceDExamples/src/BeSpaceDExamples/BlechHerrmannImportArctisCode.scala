package BeSpaceDExamples

import BeSpaceDCore.CoreDefinitions;
import BeSpaceDCore._;

object BlechHerrmannImportArctisCode extends CoreDefinitions {

	  val robotinvariantAbs1 = BIGAND ((OccupyBox(100,140,1100,160))::Nil);
	  val robotinvariantAbs2 = BIGAND ((OccupyBox(100,140,1100,160))::(BIGOR((ComponentState("FAST"))::(ComponentState("SLOW"))::(ComponentState("STANDING"))::Nil))::Nil);
	  

	  


	 
	  def robotinvariantAbs1wt() : Invariant ={
		var rai : List[Invariant] = Nil;
		for (i <- 0 to 240) {
		  
		  rai ::= (IMPLIES(TimeStamp (i),(OccupyBox(100,140,1100,160))));
		}
		return (BIGAND (rai));    
	  }
	  
	  def humanapproachingLeft() : Invariant ={
		var hali : List[Invariant] = Nil;
		for (i <- 0 to 240) {
		  hali ::= (IMPLIES(TimeStamp (i), (OccupyBox(0+(i/2),0,0+(i/2),300))));
		}
		return (BIGAND (hali));    
	  }
	 
	  
	  /*
	   * normal behavior for a robot that moves from left to right
	   */
	  def robothumaninvariantPhysicsGenNormalBehavior(startpoint : Int) : (Invariant,Invariant) ={

	    val timeraster = 5.0; // in ms 
	    val modedelay = 500.0; // in ms: pollinginterval (20ms) + modedelay (480ms) 

	    var rai : List[Invariant] = Nil; //robotinvariant
	    var hi : List[Invariant] = Nil; //humaninvariant

	// Variables standard
		var robotTSOB : (Int, Int, Int, Int, Int) = (0,99,139,121,161);
		var humanTSOB : (Int, Int, Int, Int, Int) = (0,500,-5,510,5);

		var mode : Int = 0;
		var newmode : Int = 0;
		var newt : Int = 0;

	// Variables controller
//		public double hrdistance = 10000;
		var hrdistance : Double = 10000;
//		public int contrmode;
		var contrmode : Int = 0;

//		  int tsob1x = (tsob1.lx + tsob1.ux)/2;
		var robottsob1x : Int = (robotTSOB._2 + robotTSOB._4)/2;
//		  int tsob1y = (tsob1.ly + tsob1.uy)/2;
		var robottsob1y : Int = (robotTSOB._3 + robotTSOB._5)/2;
//		  int tsob2x = (tsob2.lx + tsob2.ux)/2;
		var humantsob1x : Int = (humanTSOB._2 + humanTSOB._4)/2;
//		  int tsob2y = (tsob2.ly + tsob2.uy)/2;
		var humantsob1y : Int = (humanTSOB._3 + humanTSOB._5)/2;

	// Variables Robot
//		double speed = 0;
	  	var speed : Double = 0; //robot speed
//		double d = 0;
	  	var d : Double = 0;

	// Variables Human
//		double speedh = 0;
	  	var speedh : Double = 0; //human speed	
//		double dh = 0;
	  	var dh : Double = 0; //human speed traveled


		  rai ::= (IMPLIES(TimeStamp (robotTSOB._1),(OccupyBox(robotTSOB._2,robotTSOB._3,robotTSOB._4,robotTSOB._5))));
		  hi ::= (IMPLIES(TimeStamp (humanTSOB._1),(OccupyBox(humanTSOB._2,humanTSOB._3,humanTSOB._4,humanTSOB._5))));
	  	
		  var t : Int = 0;
		while( t < 3000) {

		// Continuous behavior robot


		  if (mode == 3) { // green 
			  if (d <= 870) {
				  if (speed < 10) {
					  speed += (5 / 1000.0) * timeraster; //acceleration / s -> ms / m -> m/10
				  }
			  } else {
				  if (speed > 1) {
					  speed -= (5 / 1000.0) * timeraster;
				  }
			  }
		  }
		  
		  if (mode == 2) { // yellow 
			  if (d <= 870) {
				  if (speed > 2) {
					  speed -= (10 / 1000.0) * timeraster; //acceleration / s -> ms / m -> m/10
				  }
			  } else {
				  if (speed > 1) { // this case is a bit underspecified in the example document
					  speed -= (10 / 1000.0) * timeraster;
				  }
			  }
		  }
		  
		  if (mode == 1) { // red 
			if (speed > 0) {
				speed -= (15 / 1000.0) * timeraster; //acceleration / s -> ms / m -> m/10
			}  
		  }
		  if (d >= 1000 || speed < 0)
			  speed = 0;
			  
		  d += (speed / 1000.0) * 10.0 * timeraster;

		  robotTSOB = (robotTSOB._1 + 1, (99 + d).toInt, 139, (121 + d).toInt, 161);


		// Continuous behavior human

		  if ((dh < 140) & (humanTSOB._1 >= startpoint)) {
			  	speedh = 10.0;
			  } else {
			    speedh = 0.0;
			  }

		  humanTSOB = (humanTSOB._1 + 1, 500, (dh - 5).toInt, 510, (dh + 5).toInt);


		// Preparation for verification tool
		  
		  rai ::= (IMPLIES(TimeStamp (robotTSOB._1),(OccupyBox(robotTSOB._2,robotTSOB._3,robotTSOB._4,robotTSOB._5))));
		  hi ::= (IMPLIES(TimeStamp (humanTSOB._1),(OccupyBox(humanTSOB._2,humanTSOB._3,humanTSOB._4,humanTSOB._5))));


		  // handling the discrete control

		  if (robottsob1x > 0 && humantsob1x < 1200 && humantsob1y > 0 && humantsob1y < 300)
		    hrdistance = Math.sqrt(((robottsob1y - humantsob1y) * (robottsob1y - humantsob1y)) + ((robottsob1x - humantsob1x) * (robottsob1x - humantsob1x)));
		  else
		    hrdistance = 10000;
		  if (hrdistance < 100)
			  contrmode = 1;
		  else if (hrdistance < 250)
		          contrmode = 2;
		  else
			  contrmode = 3;


		  
		  // handling the mode delay

			if (contrmode != mode && newt < 0) {
				newmode = contrmode;
				newt = robotTSOB._1.toInt + (modedelay / timeraster).toInt;
			}

			if (newt >= 0 && newt <= robotTSOB._1) {
				mode = newmode;
				newt = -1;
			}



		}
		return (BIGAND (rai),BIGAND(hi));    
	  }
	  
	    def robotinvariantPhysicsGenNormalBehaviorCoarse() : Invariant ={
	    val timeraster = 5.0; // in ms 
		var rai : List[Invariant] = Nil;
	  	var speed : Double = 0;
	  	var d : Double = 0;
	  	var t : Int = 0;
	  	rai ::= (IMPLIES(TimeStamp (t),(OccupyBox((98 + d).toInt,139,(122 + d).toInt,161))));

		while(d < 980) {
		  if (d <= 870) {
			  if (speed < 10) {
				  speed += (5.0 / 1000.0) / 10.0 * timeraster; //acceleration / s -> ms / m -> m/10
			  }
		  } else {
			  if (speed > 1) {
				  speed -= (5.0 / 1000.0) * timeraster;
			  }
		  }
		  t+=1;
		  d += (speed / 1000.0) * 10.0 * timeraster ;
		  rai ::= (IMPLIES(TimeStamp (t),(OccupyBox((98 + d).toInt,139,(122 + d).toInt,161))));

		}
		return (BIGAND (rai));    
	  }
	  
	  def main(args: Array[String]) {
	    println("Topological Invariants:");
	    println ("Geometric Invariants:");
	    for ( i<- 0 to 240) {
	    	println(prettyPrintInvariant(this.getSubInvariantForTime(i,simplifyInvariant(humanapproachingLeft)))+"   "+
	    	    prettyPrintInvariant(this.getSubInvariantForTime(i,simplifyInvariant(robotinvariantAbs1wt))));
	    	
	  	}
	    //println (this.robotinvariantPhysicsGenNormalBehavior);
	    println("Normal Physics behavior");
	    println(prettyPrintInvariant(this.getSubInvariantForTime(10,(this.robothumaninvariantPhysicsGenNormalBehavior(1000)) match {case (r,h)=>r})));
	    
	    //var t : Int = 0;
	    for ( t <- 0 to 2000) {
	          println(t*5 + "ms : " + this.getSubInvariantForTime(t,(this.robothumaninvariantPhysicsGenNormalBehavior(1000)) match {case (r,h)=>r}));

	    }
	    
	     println (this.collisionTestsAlt1( 
	            this.simplifyInvariant(this.unfoldInvariant(getSubInvariantForTime(1,(this.robothumaninvariantPhysicsGenNormalBehavior(1000)) match {case (r,h)=>r})))::Nil, 
	            this.simplifyInvariant(this.unfoldInvariant(getSubInvariantForTime(1,(this.robothumaninvariantPhysicsGenNormalBehavior(1000)) match {case (r,h)=>h})))::Nil));
	     
	     var result : Boolean = false;
	     var time = System.currentTimeMillis();
	     for (tstart <- 1 to 1000) {
	       
	    	 for (t <- 1 to 1000) {
	    		 result &=
	    		 this.collisionTestsAlt1( 
	    				 this.simplifyInvariant(this.unfoldInvariant(getSubInvariantForTime(t,(this.robothumaninvariantPhysicsGenNormalBehavior(tstart)) match {case (r,h)=>r})))::Nil, 
	    				 this.simplifyInvariant(this.unfoldInvariant(getSubInvariantForTime(t,(this.robothumaninvariantPhysicsGenNormalBehavior(tstart)) match {case (r,h)=>h})))::Nil);
	     
	       
	    	 }      
	     }
	     
	      println("Time needed :"+ (System.currentTimeMillis() - time) + "milliseconds");
	     println ("final result "+result);


	    
	       
	    //println(this.getSubInvariantForTime(1,this.robotinvariantPhysicsGenNormalBehavior));


	      //  println (this.collisionTestsAlt1( 
	     //       this.simplifyInvariant(this.unfoldInvariant(getSubInvariantForTime(i,invari()))):: this.simplifyInvariant(this.unfoldInvariant(getSubInvariantForTime(i,invariant1bgeneration())))::Nil, 
	    //        this.simplifyInvariant(this.unfoldInvariant(getSubInvariantForTime(i,invariant2bgeneration()))):: this.simplifyInvariant(this.unfoldInvariant(getSubInvariantForTime(i,invariant2ageneration())))::Nil));
	        
	    //    println (this.collisionTestsAlt1( 
	    //        this.simplifyInvariant(this.unfoldInvariant(getSubInvariantForTime(i,invariant1aBIGANDgeneration()))):: this.simplifyInvariant(this.unfoldInvariant(getSubInvariantForTime(i,invariant1bBIGANDgeneration())))::Nil, 
	    //        this.simplifyInvariant(this.unfoldInvariant(getSubInvariantForTime(i,invariant2bBIGANDgeneration()))):: this.simplifyInvariant(this.unfoldInvariant(getSubInvariantForTime(i,invariant2aBIGANDgeneration())))::Nil));

	    	//println (this.collisionTests( getSubInvariantForTime(i,invariant2bgeneration()) :: Nil, getSubInvariantForTime(i,invariant2bgeneration())::Nil));
	    
	  }
}