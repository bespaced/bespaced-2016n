/*
 * LICENSED under Apache 2.0 (http://directory.fsf.org/wiki/License:Apache2.0)
 */

/**
 * @author keith
 */

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.List;
import java.util.Locale;

// OPC Foundation
import org.opcfoundation.ua.builtintypes.DataValue;
import org.opcfoundation.ua.builtintypes.LocalizedText;
import org.opcfoundation.ua.builtintypes.ExpandedNodeId;
import org.opcfoundation.ua.builtintypes.NodeId;
import org.opcfoundation.ua.builtintypes.UnsignedInteger;
import org.opcfoundation.ua.common.ServiceResultException;
import org.opcfoundation.ua.core.ApplicationDescription;
import org.opcfoundation.ua.core.ApplicationType;
import org.opcfoundation.ua.core.Attributes;
import org.opcfoundation.ua.core.Identifiers;
import org.opcfoundation.ua.core.ReferenceDescription;
import org.opcfoundation.ua.core.NodeClass;
import org.opcfoundation.ua.transport.security.SecurityMode;
import org.opcfoundation.ua.utils.AttributesUtil;

// Prosys OPC UA
import com.prosysopc.ua.ApplicationIdentity;
import com.prosysopc.ua.SecureIdentityException;
import com.prosysopc.ua.ServiceException;
import com.prosysopc.ua.StatusException;
import com.prosysopc.ua.client.AddressSpace;
import com.prosysopc.ua.client.AddressSpaceException;
import com.prosysopc.ua.client.ServerConnectionException;
import com.prosysopc.ua.client.UaClient;
import com.prosysopc.ua.nodes.UaNode;
import com.prosysopc.ua.nodes.UaReferenceType;
import com.prosysopc.ua.nodes.UaType;



package object BeSpaceDOPC {
  
  @Deprecated
  def printReferences(references: java.util.List[org.opcfoundation.ua.core.ReferenceDescription], client: com.prosysopc.ua.client.UaClient) = 
  {
    for (i <- 0 until references.size)
      {
      System.out.println(i + " - " + referenceToString(client , references.get(i)));
      }
  }
  
  
  /**
  * @param r
  * @return
  * @throws ServiceException
  * @throws ServerConnectionException
  * @throws StatusException
  */
  def referenceToString(client: UaClient, r: ReferenceDescription): String =
     //throws ServerConnectionException, ServiceException, StatusException 
   {
   if (r == null) return ""
     
   var referenceTypeStr: String = null
   
   try
   {
     // Find the reference type from the NodeCache
     val nodeType: UaType = client.getAddressSpace().getType(r.getReferenceTypeId());
     if ((nodeType != null) && (nodeType.getDisplayName != null))
       if (r.getIsForward)
         referenceTypeStr = nodeType.getDisplayName.getText
       else
       {
         val referenceType: UaReferenceType = nodeType.asInstanceOf[UaReferenceType]
         referenceTypeStr = referenceType.getInverseName.getText
       }
   } 
   catch  
   {
     case e: AddressSpaceException =>
       System.out.println(e);
       System.out.println(r.toString());
       referenceTypeStr = r.getReferenceTypeId().getValue().toString();
   }
   
   var typeStr: String = null
   
   val nodeClass: NodeClass = r.getNodeClass()
   
   nodeClass match 
   {
   case NodeClass.Object | NodeClass.Variable =>
     try {
       val node: UaNode = client.getAddressSpace().getNode(r.getTypeDefinition())
       if (node != null)
         typeStr = node.getDisplayName().getText()
       else
         typeStr = r.getTypeDefinition().getValue().toString()
     } 
     catch 
     {
       case e: AddressSpaceException =>
         System.out.println(e);
         System.out.println("type not found: " + r.getTypeDefinition().toString());
         typeStr = r.getTypeDefinition().getValue().toString();
     }
     
   case _ => typeStr = "[" + r.getNodeClass() + "]"
   }
   
   return String.format(
       "%s%s (ReferenceType=%s, BrowseName=%s%s)",
       r.getDisplayName().getText(),
       ": " + typeStr,
       referenceTypeStr,
       r.getBrowseName(),
       if (r.getIsForward() ) "" else " [Inverse]"
       )
  }


  /**
   * Define a minimal ApplicationIdentity. If you use secure connections, you
   * will also need to define the application instance certificate and manage
   * server certificates. See the SampleConsoleClient.initialize() for a full
   * example of that.
   */
  def initialize(client: UaClient)
      //throws SecureIdentityException, IOException, UnknownHostException 
      {
    // *** Application Description is sent to the server
    val appDescription = new ApplicationDescription();
    appDescription.setApplicationName(new LocalizedText("SimpleClient", Locale.ENGLISH));
    // 'localhost' (all lower case) in the URI is converted to the actual
    // host name of the computer in which the application is run
    appDescription.setApplicationUri("urn:localhost:UA:SimpleClient");
    appDescription.setProductUri("urn:prosysopc.com:UA:SimpleClient");
    appDescription.setApplicationType(ApplicationType.Client);

    val identity = new ApplicationIdentity();
    identity.setApplicationDescription(appDescription);
    client.setApplicationIdentity(identity);
  }

}