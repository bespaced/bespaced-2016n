/*
 * LICENSED under Apache 2.0 (http://directory.fsf.org/wiki/License:Apache2.0)
 */

/**
 * @author keith
 */


package BeSpaceDOPC.demonstrator

import java.util.List

// OPC Foundation
import org.opcfoundation.ua.builtintypes.DataValue
import org.opcfoundation.ua.builtintypes.ExpandedNodeId
import org.opcfoundation.ua.builtintypes.NodeId
import org.opcfoundation.ua.core.Attributes
import org.opcfoundation.ua.core.Identifiers
import org.opcfoundation.ua.core.ReferenceDescription
import org.opcfoundation.ua.transport.security.SecurityMode

// Prosys OPC UA
import com.prosysopc.ua.client.AddressSpace
import com.prosysopc.ua.client.UaClient

// Scala OPC Helpers
import BeSpaceDOPC._

// BeSPaceD
import BeSpaceDCore._


object WeatherDataTestUAClientToUAServerViaGateway {
  
  // NAVIGATION Constants
  val OBJECTS     = "Objects"
  val QUICKSTARTS = "OPCFoundationQuickStartsDAS"
  val TEST_DATA   = "TestData"
  val STATIC      = "Static"
  val FC1001      = "FC1001"
  val MEASUREMENT = "Measurement"
  val SETPOINT    = "SetPoint"
    
    

  def main(args: Array[String]) 
  {
    val client = connect    
    val fc1001 = navigateToFc1001(client)

    // Measurement    
    val measurement = fc1001.browse(MEASUREMENT)
    measurement.print
    
    
    // FC1001 node
    for (i <- 0 until 3)
    {
       val fc1001Value: DataValue = client.readAttribute(measurement.nodeId, Attributes.Value);
       
       println("fc1001Value: " + fc1001Value);
       Thread.sleep(1000);
    }
    
    
    // Set Points    
    val setpoint = fc1001.browse(SETPOINT)
    setpoint.print
    
  
    
    // Setup BeSpaceD Test Data
    val uvData = BIGAND(
        IMPLIES(Owner("Melbourne"), ComponentState(4.8)),
        IMPLIES(Owner("Melbourne"), ComponentState(4.6)),
        IMPLIES(Owner("Melbourne"), ComponentState(4.3)),
        IMPLIES(Owner("Melbourne"), ComponentState(3.9)),
        IMPLIES(Owner("Melbourne"), ComponentState(3.8)),
        IMPLIES(Owner("Melbourne"), ComponentState(3.1)),
        IMPLIES(Owner("Melbourne"), ComponentState(4.2)),
        IMPLIES(Owner("Melbourne"), ComponentState(4.6)),
        IMPLIES(Owner("Melbourne"), ComponentState(5.4)),
        IMPLIES(Owner("Melbourne"), ComponentState(5.0)),
        IMPLIES(Owner("Melbourne"), ComponentState(6.3)),
        IMPLIES(Owner("Melbourne"), ComponentState(7.1)),
        IMPLIES(Owner("Melbourne"), ComponentState(7.5))
        )
    
      println("BeSpaceD UV Data: " + uvData)
      
      println("Writing UV Data into OPC UA Server ...")
    
    // FC1001 node
    for (i <- 0 until uvData.terms.size)
    {
      // READ from BeSpaceD
      val uvIndex = uvData.terms(i).conclusion.state
        
      // WRITE to OPC UA
      setpoint.writeValue(uvIndex.toFloat)
      Thread.sleep(1000)

      val fc1001ReadValue = setpoint.readValue
       
      println("fc1001Value read: " + fc1001ReadValue)
    }
    
    client.disconnect();
  }

  def navigateToFc1001(client: com.prosysopc.ua.client.UaClient): BeOpcUaNode = 
  {   
    // Address Space    
    val root = BeOpcUaNode.root(client)
    root.print
    

    // Objects
    val objects = root.browse(OBJECTS)
    objects.print

    
    // QuickStarts
    val quickstarts = objects.browse(QUICKSTARTS)
    quickstarts.print


    // Test Data
    val testData = quickstarts.browse(TEST_DATA)
    testData.print

    
    // Static
    val static = testData.browse(STATIC)
    static.print

    
    // Device
    val fc1001 = static.browse(FC1001)
    fc1001.print
    
    fc1001
  }
  
  def navigateToSetpoint(client: com.prosysopc.ua.client.UaClient): BeOpcUaNode =
  {
        val fc1001 = navigateToFc1001(client)
        val setpoint = fc1001.browse(SETPOINT)

        setpoint
  }

  def connect: UaClient = 
  {
    //UaClient client = new UaClient("opc.tcp://localhost:52520/OPCUA/SampleConsoleServer");
    
    // OPC UA Motor Simulation
    val client: UaClient = new UaClient("opc.tcp://10.234.2.208:4850/Quickstarts/DataAccessServer");
    
    client.setSecurityMode(SecurityMode.NONE);
    initialize(client);
    client.connect();
    
    // Server State
    println("Server status...");
    val value: DataValue = client.readValue(Identifiers.Server_ServerStatus_State);
    println(value);
    
    client
  }
}