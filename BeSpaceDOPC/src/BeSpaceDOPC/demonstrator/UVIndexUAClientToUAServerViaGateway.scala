/*
 * LICENSED under Apache 2.0 (http://directory.fsf.org/wiki/License:Apache2.0)
 */

/**
 * @author keith
 */


package BeSpaceDOPC.demonstrator

import java.util.List

// OPC Foundation
import org.opcfoundation.ua.builtintypes.DataValue
import org.opcfoundation.ua.builtintypes.ExpandedNodeId
import org.opcfoundation.ua.builtintypes.NodeId
import org.opcfoundation.ua.core.Attributes
import org.opcfoundation.ua.core.Identifiers
import org.opcfoundation.ua.core.ReferenceDescription
import org.opcfoundation.ua.transport.security.SecurityMode

// Prosys OPC UA
import com.prosysopc.ua.client.AddressSpace
import com.prosysopc.ua.client.UaClient

// Scala OPC Helpers
import BeSpaceDOPC._


object UVIndexUAClientToUAServerViaGateway {
  
  // NAVIGATION Constants
  val OBJECTS     = "Objects"
  val MY_OBJECTS     = "MyObjects"
  val MY_BIG_NODE_MANAGER = "MyBigNodeManager"
  val WEATHER     = "WEATHER"
  val MEASUREMENT = "Measurement"
  val SETPOINT    = "SetPoint"
    
    
  def main(args: Array[String]) 
  {
    val client = connect
    
    val weather = navigateToWeather(client)

        
    // UVIndex node
    for (i <- 0 until 10)
    {
       val uvIndex: DataValue = client.readAttribute(weather.nodeId, Attributes.Value)
       
       println("uvIndex: " + uvIndex);
       Thread.sleep(1000)
    }
    
        
    // Write UV Index values into FC1001 node
    for (i <- 0 until 10)
    {
      // Get UV Index from BeSpaceD
      
      
      // Write data to OPC UA address space
      weather.writeValue((i*5).toFloat)
      Thread.sleep(1000)

      val uvIndex = weather.readValue
       
      println("uvIndex read: " + uvIndex)
    }

    
    // Write UV Index measurement values into FC1001 node
//    val uvIndex = 100
//    
//    for (i <- 0 until 10)
//    {
//      measurement.writeValue(uvIndex.toFloat)
//      Thread.sleep(1000)
//
//      val fc1001ReadValue = measurement.readValue
//       
//      println("fc1001Value read: " + fc1001ReadValue)
//    }

        
    
    client.disconnect();
  }

  def navigateToWeather(client: UaClient): BeOpcUaNode = 
  {
    // Address Space    
    val root = BeOpcUaNode.root(client)
    root.print
    

    // Objects
    val objects = root.browse(OBJECTS)
    objects.print

    
    // MyBigNodeManager
//    val bigManager = objects.browse(MY_BIG_NODE_MANAGER)
//    bigManager.print

        
    // My Objects
    val myObjects = objects.browse(MY_OBJECTS)
    myObjects.print

    
    // Weather
    val weather = myObjects.browse(WEATHER)
    weather.print
    
    weather
  }

  def connect: UaClient = 
  {
    //UaClient client = new UaClient("opc.tcp://localhost:52520/OPCUA/SampleConsoleServer");
    
    // OPC UA Motor Simulation
    //val client: UaClient = new UaClient("opc.tcp://10.234.2.208:4850/Quickstarts/DataAccessServer");
    
    // Local Sample Server
    val client: UaClient = new UaClient("opc.tcp://localhost:52520/OPCUA/SampleConsoleServer")
    
    client.setSecurityMode(SecurityMode.NONE);
    initialize(client);
    client.connect();
    
    // Server State
    println("Server status...");
    val value: DataValue = client.readValue(Identifiers.Server_ServerStatus_State);
    println(value);
    
    client
  }
}