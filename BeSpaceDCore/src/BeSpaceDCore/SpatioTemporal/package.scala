package BeSpaceDCore

/**
 * Created by keith on 6/02/16.
 */


package object SpatioTemporal {

  // Time
  class IntegerTime(number: Int) extends TotalTimeOrdered[Int](number)(ev = { i:Int => Ordering[Int] })
  {
    import IntegerTime._

    def +(step: Step): IntegerTime = new IntegerTime(number = number + step)
  }

  object IntegerTime
  {
    type Step = Int
  }


  // Time Orderings
  implicit val intTimeOrdering = { _: Int => Ordering[Int] } 
  
  
  
  // Fold Time
  def foldTime[T]  = FoldTime.foldTime[T]   _
  def foldTime2[A](
                    invariant: Invariant,
                    accumulator: A,
                    startTime: IntegerTime,
                    stopTime: IntegerTime,
                    step: IntegerTime.Step,
                    f: (A, Invariant) => A
                    ): A = FoldTime.foldTime2[A](invariant, accumulator, startTime, stopTime, step, f)


  // Fold Space
  type Translation = FoldSpace.Translation
  def foldSpace[T] = FoldSpace.foldSpace[T] _


  // Map Time
  def mapTime[T <: Invariant]        = MapTime.mapTime[T] _
  def filterMapTime[T <: Invariant]  = MapTime.filterMapTime[T] _

  // Filter Time
  def filterTime(startTime: IntegerTime, stopTime: IntegerTime, defaultReturn: Invariant)(invariant: Invariant): Invariant  = FilterTime.filterTime(startTime, stopTime, defaultReturn)(invariant) 
  def filterTime2(startTime: IntegerTime, stopTime: IntegerTime, successReturn: BOOL)(invariant: Invariant): Invariant = FilterTime.filterTime2(startTime, stopTime, successReturn)(invariant)

}
