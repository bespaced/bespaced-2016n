/*
 * J O Blech 2014
 * 
 * Started implementing and developing : 19/05/2014, fixed 21/05/2014 and onwards
 * The idea is to look at plaines containing points associated. Each point is associated with a probability
 */


package BeSpaceDCore

class ProbabilisticEvaluator {
  
	var maxX : Int = 0
	var minX : Int = 0
	var maxY : Int = 0
	var minY : Int = 0
	
	var planesMaxProb : Map[(Int,Int), Double] = Map() //Map((0,0) -> 1.0)
	var planesMinProb : Map[(Int,Int), Double] = Map()
	
	def setPlanesMaxProb () { // complete soon
	  
	}
	
	//idea: returns probability plane of a collision based on given planes p1 and p2
	//written 20/05/2014: tested 21/05/2014
	def intersect (p1 : ProbabilisticEvaluator, p2 : ProbabilisticEvaluator) : ProbabilisticEvaluator ={
	  var ret_planesProb = new ProbabilisticEvaluator
	  
	  ret_planesProb.maxX =  Math.max(p1.maxX,p2.maxX)
	  ret_planesProb.minX =  Math.min(p1.minX,p2.minX)
	  ret_planesProb.maxY =  Math.max(p1.maxY,p2.maxY)
	  ret_planesProb.minY =  Math.min(p1.minX,p2.minX)
	  
	  
	  for (x <- Math.min(p1.minX,p2.minX) to Math.max(p1.maxX,p2.maxX)) {
		  for (y <- Math.min(p1.minY,p2.minY) to Math.max(p1.maxY,p2.maxY)) {
			  ret_planesProb.planesMaxProb += ((x,y) -> (p1.planesMaxProb.getOrElse((x,y),0.0d) * p2.planesMaxProb.getOrElse((x,y),0.0d)))
			  ret_planesProb.planesMinProb += ((x,y) -> (p1.planesMinProb.getOrElse((x,y),0.0d) * p2.planesMinProb.getOrElse((x,y),0.0d)))
		  }	    
	  }
	  
	  return ret_planesProb
	}
	
    //idea: returns probability plane of a e.g., coverage of sensors (at least one out of two) ranges based on given planes p1 and p2
	//written 22/05/2014: fixed and tested 23/05/2014
	def neg_intersect (p1 : ProbabilisticEvaluator, p2 : ProbabilisticEvaluator) : ProbabilisticEvaluator ={
	  var ret_planesProb = new ProbabilisticEvaluator
	  
	  ret_planesProb.maxX =  Math.max(p1.maxX,p2.maxX)
	  ret_planesProb.minX =  Math.min(p1.minX,p2.minX)
	  ret_planesProb.maxY =  Math.max(p1.maxY,p2.maxY)
	  ret_planesProb.minY =  Math.min(p1.minX,p2.minX)
	  
	  
	  for (x <- Math.min(p1.minX,p2.minX) to Math.max(p1.maxX,p2.maxX)) {
		  for (y <- Math.min(p1.minY,p2.minY) to Math.max(p1.maxY,p2.maxY)) {
			  ret_planesProb.planesMaxProb += ((x,y) -> (1.0d - ((1.0d - p1.planesMaxProb.getOrElse((x,y),1.0d)) * (1.0d - p2.planesMaxProb.getOrElse((x,y),1.0d)))))
			  ret_planesProb.planesMinProb += ((x,y) -> (1.0d - ((1.0d - p1.planesMinProb.getOrElse((x,y),1.0d)) * (1.0d - p2.planesMinProb.getOrElse((x,y),1.0d)))))
		  }	    
	  }
	  
	  return ret_planesProb
	}
	
	
	
	//added: 02/06/2014
	//returns probability sum in plane
	//untested
	def probsumMax (p1 : ProbabilisticEvaluator) : Double ={
	  var d = 0.0d
	  for (x <- p1.minX to p1.maxX) {
		  for (y <- p1.minY to p1.maxY) {
			  d += p1.planesMaxProb(x,y)
		  }	    
	  }	  
	  return d
	}

	//added: 02/06/2014
	//returns probability sum in plane
	//untested
	def probsumMin (p1 : ProbabilisticEvaluator) : Double ={
	  var d = 0.0d
	  for (x <- p1.minX to p1.maxX) {
		  for (y <- p1.minY to p1.maxY) {
			  d += p1.planesMinProb(x,y)
		  }	    
	  }	  
	  return d
	}
}